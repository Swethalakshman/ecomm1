﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Business
    {
        UnitOfWork unitOfWork;
        public Business(IHttpContextAccessor httpContextAccessor)
        {
            unitOfWork = httpContextAccessor.HttpContext.RequestServices.GetService(typeof(UnitOfWork)) as UnitOfWork;
        }
        public List<Category> GetCategories()
        {
            var rec = unitOfWork.Repository<Category>().Set().ToList();
            return rec;
        }
        public List<Productinfo> GetProducts()
        {
            var rec = unitOfWork.Repository<Productinfo>().Set().ToList();
            return rec;
        }
        public int SaveUserByMobileNumber(string MobileNumber)
        {
            var success = -1;
            try
            {
                
                UserMaster user = new UserMaster();
                user.MobileNo = MobileNumber.Trim();
                var isExistUser = unitOfWork.Repository<UserMaster>().Set().Where(e => e.MobileNo == MobileNumber).FirstOrDefault();
                if (isExistUser == null)
                {
                    UserRoleMap map = new UserRoleMap();
                    unitOfWork.BeginTransaction();
                    unitOfWork.Repository<UserMaster>().Insert(user);
                    unitOfWork.SaveChanges();

                    map.UserId = user.Id;
                    map.RoleId = 2;
                    unitOfWork.Repository<UserRoleMap>().Insert(map);
                    unitOfWork.SaveChanges();
                    unitOfWork.Commit();
                    success = 1;
                }
                else if(isExistUser.Id > 0)
                {
                    success = 1;
                }
            }
            catch (Exception e)
            {
                unitOfWork.Rollback();
            }
            return success;

        }
    }
}
