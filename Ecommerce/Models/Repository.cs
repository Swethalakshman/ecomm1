﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecommerce.Models
{
    public class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<Entity> _dbSet;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public Repository(DbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = context;
            _dbSet = context.Set<Entity>();
            _httpContextAccessor = httpContextAccessor;
        }

        public void BulkInsert(IEnumerable<Entity> entities)
        {
            _dbSet.AddRange(entities);
        }
        public void Detach(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
            {
                _dbContext.Entry(entity).State = EntityState.Detached;
            }
        }
        public void MarkAsUnModified(string propertyName)
        {
            foreach (EntityEntry<Entity> entry in _dbContext.ChangeTracker.Entries<Entity>())
            {
                if (entry.State == EntityState.Modified)
                {
                    entry.Property(propertyName).IsModified = false;
                }
            }
        }

        public void BulkUpdate(IEnumerable<Entity> entities)
        {
            _dbSet.UpdateRange(entities);
        }

        public void BulkDelete(IEnumerable<Entity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public void Delete(Entity entity)
        {
            _dbSet.Remove(entity);
        }

        public void DeleteById(object id)
        {
            Entity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public Entity GetById(object id)
        {
            Entity entity = null;
            var dataToReturn = _dbSet.Find(id);
            entity = dataToReturn;
            return entity;
        }


        public void Insert(Entity entity)
        {
            _dbSet.Add(entity);
        }

        //public IQueryable<Entity> Set()
        //{
        //        return _dbSet.Where("IsActive==" + true);

        //}

        public void Update(Entity entity)
        {

            _dbSet.Update(entity);
        }

        public IQueryable<Entity> Set()
        {
            return _dbSet;
        }
    }
}
